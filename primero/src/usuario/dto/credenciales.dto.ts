import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty, IsString, MinLength } from "class-validator";

export class CredencialesDTO {

    @ApiProperty({
        example: 'user_name',
        required: true,
      })
    @IsString()
    @IsNotEmpty()
    @IsEmail()
    nombreUsuario: string;

    @ApiProperty({
        example: 'password',
        required: true,
      })
    @IsString()
    @IsNotEmpty()
    @MinLength(6 , {
        message: 'El password deberia tener al menos 6 caracteres',
    }
    )
    password: string;
}