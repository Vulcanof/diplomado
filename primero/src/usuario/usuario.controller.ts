import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { UsuarioService } from './usuario.service';
import { CreateUsuarioDto } from './dto/create-usuario.dto';
import { UpdateUsuarioDto } from './dto/update-usuario.dto';
import { CredencialesDTO } from './dto/credenciales.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth-guard';
import { ApiBearerAuth, ApiBody, ApiResponse, ApiTags } from '@nestjs/swagger';

@ApiTags('Usuario controller')
@UseGuards(JwtAuthGuard)
@Controller('usuarios')
export class UsuarioController {
  constructor(private readonly usuarioService: UsuarioService) {}

  @ApiResponse({ status: 201, description: 'Usuario creado exitosamente el id es el numero de registro' })
  @ApiResponse({ status: 401, description: 'No Autorizado' })
  @ApiResponse({ status: 403, description: 'Usuario no encontrado' })
  @ApiResponse({ status: 409, description: 'Usuario ya existente' })
  @Post()
  @ApiBody({
    type: CreateUsuarioDto,
    description: 'Estructura JSON para el objeto CreateUsuarioDto',
  })
  async create(@Body() createUsuarioDto: CreateUsuarioDto) {
    return await this.usuarioService.create(createUsuarioDto);
  }

  @ApiResponse({ status: 201, description: 'Usuario encontrado y retornado' })
  @ApiResponse({ status: 401, description: 'No Autorizado' })
  @ApiResponse({ status: 403, description: 'Usuario no encontrado' })
  @Get('/:id')
  @ApiBody({
    description: 'Buscar Usuarios ID',
  })
  findOne(@Param('id') id: number) {
    return this.usuarioService.findOne(id);
  }

  @ApiResponse({ status: 201, description: 'Lista de usuarios' })
  @ApiResponse({ status: 401, description: 'No Autorizado' })
  @ApiResponse({ status: 403, description: 'sin usuarios' })
  @Get()
  @ApiBody({
    description: 'listar todos los usuarios',
  })
  async findAll() {
    return await this.usuarioService.findAll();
  }

  @ApiResponse({ status: 201, description: 'Usuario modificado' })
  @ApiResponse({ status: 401, description: 'No Autorizado' })
  @ApiResponse({ status: 403, description: 'no se encontro el usuario' })
  @Patch('/:id')
  async update(
    @Param('id') id: number,
    @Body() updateUsuarioDto: UpdateUsuarioDto,
  ) {
    return this.usuarioService.actualizar(id, updateUsuarioDto);
  }

  @ApiResponse({ status: 201, description: 'Usuario eliminado' })
  @ApiResponse({ status: 401, description: 'No Autorizado' })
  @ApiResponse({ status: 403, description: 'no se encontro el usuario' })
  @Delete('/:id')
  async remove(@Param('id') id: number) {
    return await this.usuarioService.eliminar(id);
  }

  /*@Post('/:id')
  async crearTarea(@Param('id') id: number, @Body() tarea: TareaDto) {
    return await this.usuarioService.crearTarea(id, tarea);
  }

  @Get('/:id/listar-tareas')
  async listarTareas(
    @Param('id') id: number,
    @Body() paginacion: PaginacionDto,
  ) {
    return await this.usuarioService.listarTareas(id, paginacion);
  }

  @Get('/imagen/gatos')
  async gatos() {
    return await this.usuarioService.gatos();
  }*/
}