import { ConflictException, Injectable, UnauthorizedException } from '@nestjs/common';
import { CreateUsuarioDto } from './dto/create-usuario.dto';
import { UsuarioRepository } from './usuario.repository';
import { UpdateUsuarioDto } from './dto/update-usuario.dto';
import { Usuario } from './entities/usuario.entity';

@Injectable()
export class UsuarioService {
  
  constructor(private readonly usuarioRepository: UsuarioRepository) {}

  async create(createUsuarioDto: CreateUsuarioDto): Promise<Usuario> {
    const usuarioExistente = await this.usuarioRepository.buscarPorNombre(
      createUsuarioDto.nombreUsuario,
    );
    console.log('check', usuarioExistente);
    if (usuarioExistente) {
      throw new ConflictException('Usuario Existente');
    }
    return this.usuarioRepository.crear(createUsuarioDto);
  }

  async validarUsuario(nombreUsuario: string, password: string): Promise<Usuario> {
    const usuarioExistente= await this.usuarioRepository.buscarPorNombre(nombreUsuario);
    console.log('Usuario Existente', usuarioExistente)
    if( !usuarioExistente || usuarioExistente.password !== password && usuarioExistente.nombreUsuario !== nombreUsuario ){
      throw new UnauthorizedException(
        'Nombre de Usuario o Contrasena incorrectos'
      );
    }
    return usuarioExistente;
  }

  findOne(id: number) {
    return this.usuarioRepository.buscarPorId(id);
  }

  findAll() {
    const resultado = this.usuarioRepository.listar();
    return resultado;
  }

  actualizar(id: number, updateUsuarioDto: UpdateUsuarioDto) {
    const usuario = this.usuarioRepository.buscarPorId(id);
    if(!usuario){
      throw new Error('usuario con id ${id} no encontrado');
    }
    return this.usuarioRepository.actualizar(id, updateUsuarioDto);
  }

  eliminar(id: number) {
    return this.usuarioRepository.eliminar(id);
  }
}
