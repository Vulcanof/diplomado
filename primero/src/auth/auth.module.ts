import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Usuario } from 'src/usuario/entities/usuario.entity';
import { UsuarioModule } from 'src/usuario/usuario.module';
import { JwtModule } from '@nestjs/jwt';
import { UsuarioService } from 'src/usuario/usuario.service';
import { UsuarioRepository } from 'src/usuario/usuario.repository';
import { jwtConstants } from 'src/constants/constants';
import { JwtStrategy } from './jwt.strategy';

@Module({
    imports: [TypeOrmModule.forFeature([Usuario]), UsuarioModule, JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '60 days'}
    })],
  providers: [AuthService, UsuarioService, UsuarioRepository, JwtStrategy],
  controllers: [AuthController]
})
export class AuthModule {}
