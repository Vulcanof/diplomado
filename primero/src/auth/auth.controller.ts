import { Body, Controller, Post } from '@nestjs/common';
import { CredencialesDTO } from 'src/usuario/dto/credenciales.dto';
import { UsuarioService } from 'src/usuario/usuario.service';
import { AuthService } from './auth.service';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

@ApiTags('Login controller')
@Controller('auth')
export class AuthController {

    constructor(private usuarioService: UsuarioService, private authService:AuthService){}

    //@ApiResponse({ status: 201, description: 'Usuario creado exitosamente' })
    @ApiResponse({ status: 401, description: 'No tiene permiso de logeo contactese con sistemas' })
    //@ApiResponse({ status: 403, description: 'Usuario no encontrado' })
    @Post('login')
    async login(@Body() credencialesDTO: CredencialesDTO) {
        console.log('credenciales del cuerpo', credencialesDTO)
        const usuario = await this.usuarioService.validarUsuario(
            credencialesDTO.nombreUsuario, 
            credencialesDTO.nombreUsuario,
        );
        return this.authService.login(usuario);
    }
}
