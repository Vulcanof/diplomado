import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsString } from 'class-validator';

export class TareaDto {

  @ApiProperty({
    example: 'titulo',
  })
  @IsString()
  titulo: string;

  @ApiProperty({
    example: 'descripcion',
  })
  @IsString()
  descripcion: string;

  @ApiProperty({
    example: 'estado',
  })
  @IsString()
  estado: string;

  @ApiProperty({
    example: 'idUsuario',
  })
  @IsNumber()
  idUsuario: number;
}
