import { Injectable } from '@nestjs/common';
import { TareaRepository } from './tarea.repository';
import { Tarea } from 'src/usuario/entities/tarea.entity.ts';
import { TareaDto } from './tarea.dto';

@Injectable()
export class TareaService {
  constructor(private tareaRepository: TareaRepository) {}
  async crear(id: number, tarea: TareaDto): Promise<Tarea> {
    return await this.tareaRepository.crear(id, tarea);
  }
}
