import { Injectable } from "@nestjs/common";
import { CarreraDto } from "src/usuario/dtoUniv/carrera.dto";
import { EstudianteDto } from "src/usuario/dtoUniv/estudiante.dto";
import { FacultadDto } from "src/usuario/dtoUniv/facultad.dto";
import { PersonaDto } from "src/usuario/dtoUniv/persona.dto";
import { Carrera } from "src/usuario/entities/carrera.entity";
import { Estudiante } from "src/usuario/entities/estudiante.entity";
import { Facultad } from "src/usuario/entities/facultad.entity";
import { Persona } from "src/usuario/entities/persona.entity";
import { DataSource } from "typeorm";

@Injectable()
export class UniversidadRepository{
    constructor(private dataSource: DataSource) {}

    async crearPersona(persona: PersonaDto): Promise<Persona> {
        return await this.dataSource.getRepository(Persona).save({
          idPersona: persona.idPersona,
          ci: persona.ci,
          apellidos: persona.apellidos,
          nombres: persona.nombres,
        });
      }

    async crearEstudiante(idCarrera: string, idPersona: string , estudiante: EstudianteDto): Promise<Estudiante> {
        return await this.dataSource.getRepository(Estudiante).save({
          gestion: estudiante.gestion,
          idCarrera: idCarrera,
          idPersona: idPersona,
        });
      }

    async crearCarrera(idFacultad: number, carrera: CarreraDto): Promise<Carrera> {
      return await this.dataSource.getRepository(Carrera).save({
        idCarrera: carrera.idCarrera,
        carrera: carrera.carrera,
        id: idFacultad,
      });
    }

    async crearFacultad(facultad: FacultadDto): Promise<Facultad> {
        return await this.dataSource.getRepository(Facultad).save({
          facultad: facultad.facultad,
        });
      }
    
}