import { Injectable } from '@nestjs/common';
import { UniversidadRepository } from './universidad.repository';
import { CarreraDto } from 'src/usuario/dtoUniv/carrera.dto';
import { Carrera } from 'src/usuario/entities/carrera.entity';
import { FacultadDto } from 'src/usuario/dtoUniv/facultad.dto';
import { Facultad } from 'src/usuario/entities/facultad.entity';
import { Estudiante } from 'src/usuario/entities/estudiante.entity';
import { EstudianteDto } from 'src/usuario/dtoUniv/estudiante.dto';
import { Persona } from 'src/usuario/entities/persona.entity';
import { PersonaDto } from 'src/usuario/dtoUniv/persona.dto';

@Injectable()
export class UniversidadService {
    
    constructor(private universidadRepository: UniversidadRepository) {}

    async crearPersona(persona: PersonaDto): Promise<Persona> {
      return await this.universidadRepository.crearPersona(persona);
    }

    async crearEstudiante(idCarrera: string, idPersona: string, estudiante: EstudianteDto): Promise<Estudiante> {
      return await this.universidadRepository.crearEstudiante(idCarrera, idPersona, estudiante);
    }

    async crearCarrera(idFacultad: number, carrera: CarreraDto): Promise<Carrera> {
      return await this.universidadRepository.crearCarrera(idFacultad, carrera);
    }

    async crearFacultad(facultad: FacultadDto): Promise<Facultad> {
      return await this.universidadRepository.crearFacultad(facultad);
    }
}
