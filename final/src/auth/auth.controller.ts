import { Body, Controller, Post } from '@nestjs/common';
import { CredencialesDTO } from '../usuario/dto/credenciales.dto';
import { UsuarioService } from '../usuario/usuario.service';
import { AuthService } from './auth.service';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

@ApiTags('Login controller')
@Controller('auth')
export class AuthController {
  constructor(
    private usuarioService: UsuarioService,
    private authService: AuthService,
  ) {}

  @ApiResponse({ status: 401, description: 'No tiene permiso de logeo contactese con sistemas' })
  @Post('login')
  async login(@Body() credencialesDTO: CredencialesDTO) {
    console.log('credenciales del cuerpo', credencialesDTO);
    const usuario = await this.usuarioService.validarUsuario(
      credencialesDTO.nombreUsuario,
      credencialesDTO.password,
    );
    return this.authService.login(usuario);
  }
}
