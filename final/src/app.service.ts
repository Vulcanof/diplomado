import { Injectable } from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@ApiTags('Bienvenido')
@Injectable()
export class AppService {

  @ApiBody({
    description: 'Status del servicio',
  })
  getHello(): string {
    return 'Servicio activo';
  }
}
