import { Module } from '@nestjs/common';
import { UsuarioService } from './usuario.service';
import { UsuarioController } from './usuario.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Usuario } from './entities/usuario.entity';
import { UsuarioRepository } from './usuario.repository';
import { TareaModule } from 'src/tarea/tarea.module';
import { CatmoduleModule } from 'src/catmodule/catmodule.module';
import { Perfil } from './entities/perfil.entity.ts';
import { PerfilService } from './perfil.service';
import { PerfilRepository } from './perfil.repository';
import { Facultad } from './entities/facultad.entity';
import { Carrera } from './entities/carrera.entity';
import { Estudiante } from './entities/estudiante.entity';
import { Persona } from './entities/persona.entity';
import { UniversidadRepository } from 'src/universidad/universidad.repository';
import { UniversidadService } from 'src/universidad/universidad.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Usuario, Perfil, Facultad, Carrera, Estudiante, Persona]),
    TareaModule,
    CatmoduleModule,
  ],
  controllers: [UsuarioController],
  providers: [
    UsuarioService,
    UsuarioRepository,
    PerfilService,
    PerfilRepository,
    UniversidadRepository,
    UniversidadService
  ],
})
export class UsuarioModule {}
