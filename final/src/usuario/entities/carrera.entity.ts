import { Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryColumn } from 'typeorm';
import { Facultad } from './facultad.entity';
import { Estudiante } from './estudiante.entity';

@Entity()
export class Carrera {

  @PrimaryColumn()
  idCarrera: string;

  @Column()
  carrera: string;

  @ManyToOne(() => Facultad, (facultad) => facultad.carrera)
  @JoinColumn()
  facultad: Facultad;

  @OneToOne(() => Estudiante, (estudiante) => estudiante.carrera)
  @JoinColumn()
  estudiante: Estudiante;
}
