import { Column, Entity, OneToMany, PrimaryColumn } from "typeorm";
import { Estudiante } from "./estudiante.entity";

@Entity()
export class Persona {

  @PrimaryColumn()
  idPersona: string;

  @Column()
  ci: string;

  @Column()
  apellidos: string;

  @Column()
  nombres: string;

  @OneToMany(() => Estudiante, (estudiante) => estudiante.persona)
  estudiante: Estudiante[]

}