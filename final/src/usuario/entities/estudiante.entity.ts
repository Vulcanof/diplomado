import { Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { Carrera } from "./carrera.entity";
import { Persona } from "./persona.entity";


@Entity()
export class Estudiante {

  @PrimaryGeneratedColumn()
  idEstudiante: number;

  @Column()
  gestion: number;

  @OneToOne(() => Carrera, (carrera) => carrera.estudiante)
  @JoinColumn()
  carrera: Carrera;

  @ManyToOne(() => Persona, (persona) => persona.estudiante)
  persona: Persona;
}