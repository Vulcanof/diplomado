import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Carrera } from './carrera.entity.js';

@Entity()
export class Facultad {
  
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  facultad: string;

  @OneToMany(() => Carrera, (carrera) => carrera.facultad)
  carrera: Carrera[];

  constructor(data?: Partial<Facultad>) {
    if (data) Object.assign(this, data);
  }
}
