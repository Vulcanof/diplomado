import { PartialType } from '@nestjs/mapped-types';
import { CreateUsuarioDto } from './create-usuario.dto';
import { IsAlphanumeric, IsEmail, IsOptional, IsString, MinLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UpdateUsuarioDto extends PartialType(CreateUsuarioDto) {

  @ApiProperty({
    example: 'henrry villca',
    required: false,
  })
    @IsString()
    @IsOptional()
    @MinLength(5, {
        message: 'El nombre del usuario deberia tener al menos 5 caracteres'
    })
    nombre?: string;

    @ApiProperty({
      example: 'hvillca',
      required: false,
    })
    @IsString()
    @IsOptional()
    @MinLength(3, {
        message: 'El nickname deberia tener al menos 3 caracteres'
    })
    @IsAlphanumeric(null, { message: 'Solo se permite numeros y letras' })
    nombreUsuario?: string;

    @ApiProperty({
      example: 'mi_correo@gmail.com',
      required: false,
    })
    @IsString()
    @IsOptional()
    @IsEmail(null, {
        message: 'Ingrese un email valido'
    })
    email?: string;

    @ApiProperty({
      example: '**password**',
      required: true,
    })
    @IsString()
    @IsOptional()
    @MinLength(3, {
        message: 'El nickname deberia tener al menos 3 caracteres'
    })
    password?: string;
}
