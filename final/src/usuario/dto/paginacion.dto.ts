import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString } from 'class-validator';

export class PaginacionDto {

  @ApiProperty({
    example: 1,
    required: false,
  })
  @IsNumber()
  @IsOptional()
  limite?: number;

  @ApiProperty({
    example: 'ASC',
    required: false,
  })
  @IsString()
  @IsOptional()
  orden?: 'ASC' | 'DESC';

  @ApiProperty({
    example: 1,
    required: false,
  })
  @IsNumber()
  @IsOptional()
  page?: number;

  @ApiProperty({
    example: 'true',
    required: false,
  })
  @IsString()
  @IsOptional()
  estado?: string;
}
