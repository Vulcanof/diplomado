import { ApiProperty } from "@nestjs/swagger";
import { IsAlphanumeric, IsEmail, IsNotEmpty, IsString, MinLength } from "class-validator";

export class CreateUsuarioDto {

    @ApiProperty({
        example: 'henrry villca',
        required: true,
      })
    @IsString()
    @IsNotEmpty()
    @MinLength(5, {
        message: 'El nombre del usuario deberia tener al menos 5 caracteres',
    })
    nombre: string;

    @ApiProperty({
        example: 'hvillca',
        required: true,
      })
    @IsString()
    @IsNotEmpty()
    @MinLength(3, {
        message: 'El nickname deberia tener al menos 3 caracteres',
    })
    @IsAlphanumeric(null, { message: 'Solo se permite numeros y letras' })
    nombreUsuario: string;

    @ApiProperty({
        example: 'mi_correo@gmail.com',
        required: true,
      })
    @IsString()
    @IsNotEmpty()
    @IsEmail(null, {
        message: 'Ingrese un email valido'
    })
    email: string;

    @ApiProperty({
      example: '**password**',
        required: true,
      })
    @IsString()
    @IsNotEmpty()
    @MinLength(3, {
        message: 'El password debe tener minimamente 3 caracteres'
    })
    password: string;
}
