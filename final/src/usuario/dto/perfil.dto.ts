import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class PerfilDto {

  @ApiProperty({
    example: 'FOTO',
    required: true,
  })
  @IsString()
  @IsNotEmpty()
  foto: string;

  @ApiProperty({
    example: 'idUsuario',
    required: true,
  })
  @IsNotEmpty()
  @IsNumber()
  idUsuario: number;
}
