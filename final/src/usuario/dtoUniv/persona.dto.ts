import { ApiProperty } from "@nestjs/swagger";
import { IsAlphanumeric, IsNotEmpty, IsNumber, IsString, MinLength } from 'class-validator';

export class PersonaDto {

  @ApiProperty({
    example: 'persona1',
    required: true,
  })
  @IsString()
  @IsNotEmpty()
  idPersona: string;

    @ApiProperty({
      example: '123456-LP',
      required: true,
    })
    @IsString()
    @IsNotEmpty()
    ci: string;
  
    @ApiProperty({
      example: 'Villca',
      required: true,
    })
    @IsString()
    @IsNotEmpty()
    @MinLength(2, {
        message: 'El apellido debe tener minimamente 2 caracteres',
    })
    apellidos: string;

    @ApiProperty({
      example: 'Henrry',
      required: true,
    })
    @IsString()
    @IsNotEmpty()
    @MinLength(2, {
        message: 'El nombre debe tener minimamente 2 caracteres',
    })
    nombres: string;
  }
  