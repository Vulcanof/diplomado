import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString, isNotEmpty, isNumber } from 'class-validator';
import { Column, Entity } from "typeorm";
import { Facultad } from "../entities/facultad.entity";

export class CarreraDto {

    @ApiProperty({
      example: 'ING',
      required: true,
    })
    @IsString()
    @IsNotEmpty()
    idCarrera: string;
  
    @ApiProperty({
      example: 'Carrera de Electronica',
      required: true,
    })
    @IsNotEmpty()
    @IsString()
    @Column({ type: 'varchar', length: '75' })
    carrera: string;

  }
  