import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { Entity } from "typeorm";

export class EstudianteDto {

    @ApiProperty({
        example: 2024,
        required: true,
    })
    @IsNotEmpty()
    @IsNumber()
    gestion: number;

  }