import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { Column, Entity } from "typeorm";

export class FacultadDto {
  
    @ApiProperty({
      example: 'Facultad de Ingenieria',
      required: true,
    })
    @IsNotEmpty()
    @IsString()
    @Column({ type: 'varchar', length: '75' })
    facultad: string;
  }
  