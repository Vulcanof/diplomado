import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { UsuarioService } from './usuario.service';
import { CreateUsuarioDto } from './dto/create-usuario.dto';
import { UpdateUsuarioDto } from './dto/update-usuario.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth-guard';
import { ApiBody, ApiResponse, ApiTags } from '@nestjs/swagger';
import { TareaDto } from '../tarea/tarea.dto';
import { PaginacionDto } from './dto/paginacion.dto';
import { FacultadDto } from './dtoUniv/facultad.dto';
import { CarreraDto } from './dtoUniv/carrera.dto';
import { EstudianteDto } from './dtoUniv/estudiante.dto';
import { PersonaDto } from './dtoUniv/persona.dto';

@ApiTags('Usuario controller')
@UseGuards(JwtAuthGuard)
@Controller('usuarios')
export class UsuarioController {
  constructor(private readonly usuarioService: UsuarioService) {}

  @ApiResponse({ status: 201, description: 'Usuario creado exitosamente' })
  @ApiResponse({ status: 401, description: 'No Autorizado' })
  @ApiResponse({ status: 403, description: 'Usuario no encontrado' })
  @Post()
  @ApiBody({
    type: CreateUsuarioDto,
    description: 'Estructura JSON para el objeto CreateUsuarioDto',
  })
  async create(@Body() createUsuarioDto: CreateUsuarioDto) {
    return await this.usuarioService.create(createUsuarioDto);
  }

  @ApiResponse({ status: 201, description: 'Usuario encontrado' })
  @ApiResponse({ status: 401, description: 'No Autorizado' })
  @ApiResponse({ status: 403, description: 'Usuario no encontrado' })
  @Get('/:id')
  findOne(@Param('id') id: number) {
    return this.usuarioService.findOne(id);
  }

  @ApiResponse({ status: 201, description: 'Lista de usuarios' })
  @ApiResponse({ status: 401, description: 'No Autorizado' })
  @Get()
  async findAll() {
    return await this.usuarioService.findAll();
  }

  @ApiResponse({ status: 201, description: 'cambios realizados' })
  @ApiResponse({ status: 401, description: 'No Autorizado' })
  @ApiResponse({ status: 403, description: 'no se encontro el usuario' })
  @Patch('/:id')
  async update(
    @Param('id') id: number,
    @Body() updateUsuarioDto: UpdateUsuarioDto,
  ) {
    return this.usuarioService.actualizar(id, updateUsuarioDto);
  }

  @ApiResponse({ status: 201, description: 'usuario eliminado' })
  @ApiResponse({ status: 401, description: 'No Autorizado' })
  @ApiResponse({ status: 403, description: 'no se encontro el usuario' })
  @Delete('/:id')
  async remove(@Param('id') id: number) {
    return await this.usuarioService.eliminar(id);
  }

  @ApiResponse({ status: 201, description: 'usuario encontrado' })
  @ApiResponse({ status: 401, description: 'No Autorizado' })
  @ApiResponse({ status: 403, description: 'no se encontro el usuario' })
  @Post('/:id')
  async crearTarea(@Param('id') id: number, @Body() tarea: TareaDto) {
    return await this.usuarioService.crearTarea(id, tarea);
  }

  @ApiResponse({ status: 201, description: 'Listar tareas' })
  @ApiResponse({ status: 401, description: 'No Autorizado' })
  @ApiResponse({ status: 403, description: 'No se encontraron tareas' })
  @Get('/:id/listar-tareas')
  async listarTareas(
    @Param('id') id: number,
    @Body() paginacion: PaginacionDto,
  ) {
    return await this.usuarioService.listarTareas(id, paginacion);
  }

  @ApiResponse({ status: 201, description: 'imagen' })
  @ApiResponse({ status: 401, description: 'No Autorizado' })
  @ApiResponse({ status: 403, description: 'No encontrado' })
  @Get('/imagen/gatos')
  async gatos() {
    return await this.usuarioService.gatos();
  }

  @ApiResponse({ status: 201, description: 'Facultad registrado' })
  @ApiResponse({ status: 401, description: 'No Autorizado' })
  @ApiResponse({ status: 403, description: 'no se pudo registrar la Facultad' })
  @Post('/registrarFacultad')
  @ApiBody({
    type: FacultadDto,
    description: 'Estructura JSON para el objeto FacultadDto',
  })
  async registrarFacultad(@Body() facultad: FacultadDto) {
    return await this.usuarioService.crearFacultad(facultad);
  }

  @ApiResponse({ status: 201, description: 'Carrera registrado' })
  @ApiResponse({ status: 401, description: 'No Autorizado' })
  @ApiResponse({ status: 403, description: 'no se pudo registrar la Carrera' })
  @Post('/registrarCarrera')
  @ApiBody({
    type: CarreraDto,
    description: 'Estructura JSON para el objeto CarreraDto',
  })
  async registrarCarrera(@Param('id') id: number, @Body() carrera: CarreraDto) {
    return await this.usuarioService.crearCarrera(id, carrera);
  }

  @ApiResponse({ status: 201, description: 'Estudiante registrado' })
  @ApiResponse({ status: 401, description: 'No Autorizado' })
  @ApiResponse({ status: 403, description: 'no se pudo registrar la Estudiante' })
  @Post('/registrarEstudiante')
  @ApiBody({
    type: EstudianteDto,
    description: 'Estructura JSON para el objeto EstudianteDto',
  })
  async registrarEstudiante(@Param('idCarrera') idCarrera: string, @Param('idPersona') idPersona: string,@Body() estudiante: EstudianteDto) {
    return await this.usuarioService.crearEstudiante(idCarrera, idPersona, estudiante);
  }

  @ApiResponse({ status: 201, description: 'Persona registrado' })
  @ApiResponse({ status: 401, description: 'No Autorizado' })
  @ApiResponse({ status: 403, description: 'no se pudo registrar la persona' })
  @Post('/registrarPersona')
  @ApiBody({
    type: PersonaDto,
    description: 'Estructura JSON para el objeto PersonaDto',
  })
  async registrarPersona(@Body() persona: PersonaDto) {
    return await this.usuarioService.crearPersona(persona);
  }
}
