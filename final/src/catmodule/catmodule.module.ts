import { Module } from '@nestjs/common';
import { CatService } from './catservice.service';

@Module({
  imports: [],
  providers: [CatService],
  exports: [CatService],
})
export class CatmoduleModule {}
